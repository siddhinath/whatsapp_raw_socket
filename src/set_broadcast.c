#ifndef _SET_BROADCAST_
#define _SET_BROADCAST_ 1

#include "header_files.h"

void SetBroadcastMacAddr(struct ethhdr *eth)
{
	eth->h_dest[0]=mac_broadcast[0];
	eth->h_dest[1]=mac_broadcast[1];
	eth->h_dest[2]=mac_broadcast[2];
	eth->h_dest[3]=mac_broadcast[3];
	eth->h_dest[4]=mac_broadcast[4];
	eth->h_dest[5]=mac_broadcast[5];
}

#endif
