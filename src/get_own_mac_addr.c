#ifndef _GET_OWN_MAC_
#define _GET_OWN_MAC_ 1

#include "header_files.h"


struct ifreq  GetOwnMacAddr(int raw,char *interface)
{
	struct ifreq ifreq_c;
	memset(&ifreq_c,0,sizeof(ifreq_c));
	strncpy(ifreq_c.ifr_name,interface,IFNAMSIZ-1);//giving name of Interface
 
	if((ioctl(raw,SIOCGIFHWADDR,&ifreq_c))<0) //getting MAC Address
		printf("error in SIOCGIFHWADDR ioctl reading");

	mac_own_addr[0]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[0];
	mac_own_addr[1]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[1];
	mac_own_addr[2]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[2];
	mac_own_addr[3]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[3];
	mac_own_addr[4]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[4];
	mac_own_addr[5]=(unsigned char)ifreq_c.ifr_hwaddr.sa_data[5];

	return ifreq_c;
}

#endif
