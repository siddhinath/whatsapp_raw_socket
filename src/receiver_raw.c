//ifconfig wlp3s0 promisc	
//Program is able to accept raw_recv packets which not pass through the network stack like eth,ip,transport.
//Date:	6.4.18, 7:51PM

#include "header_files.h"

//int main(int argc,char *argv[])
void *Receiver(void *vargp)
{
	char i_name[40];
	Fetch_Interface_Name(i_name);

//	strcpy(userid,argv[1]);
//	strcat(userid,"\0");

	int bytes_read=0,x=0;
	unsigned char buffer[PKT_LEN];
	memset(buffer,0,PKT_LEN);

	//struct sockaddr_ll packet_info;
	struct sockaddr_storage inc_packet;
	struct ifreq ifr;
	socklen_t inc_packetl;

	//create socket by passing protocol field as paramenter.
	raw_recv=CreateRawSocket(ETH_P_ALL);

	/* Bind raw_recv socket to interface */
	BindRawSocketToInterface(i_name, raw_recv, ETH_P_ALL);

	inc_packetl=sizeof(struct sockaddr_storage);
	while(1)
	{
		bytes_read=recvfrom(raw_recv,buffer,PKT_LEN,0,(struct sockaddr*) & inc_packet,&inc_packetl);

		if(bytes_read==0)
			printf("Message Not Read\n");
	//printf("Packet Length=%d\n",bytes_read);
		Process_Packet(buffer);
		memset(buffer,0,PKT_LEN);
	}//while end

	close(raw_recv);
	return 0;
}//main end
