#ifndef _CREATE_SOCKET_
#define _CREATE_SOCKET_ 1

#include "header_files.h"

int CreateRawSocket(int protocol_to_raw)
{
	int rawsock;
	if((rawsock = socket(PF_PACKET, SOCK_RAW, htons(protocol_to_raw)))== -1)
	{
		perror("Error creating raw socket: ");
		exit(-1);
	}
	else
		printf("Socket Create Successfully\n");
	return rawsock;
}

#endif
