#ifndef _SEND_PACKET_
#define _SEND_PACKET_ 1

#include "header_files.h"

int SendRawPacket(int rawsock, unsigned char *pkt, int pkt_len)
{
	int sent= 0;
//	printf("Packet len: %d\n", pkt_len);

	/* A simple write on the socket ..thats all it takes ! */

	if((sent = write(rawsock, pkt, pkt_len)) != pkt_len)
	{
		/* Error */
		printf("Could only send %d bytes of packet of length %d\n", sent, pkt_len);
		return 0;
	}

	return 1;
}

#endif
