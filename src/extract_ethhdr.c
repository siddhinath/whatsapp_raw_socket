#ifndef _EXTRACT_ETH_HDR_
#define _EXTRACT_ETH_HDR_ 1

#include "header_files.h"


//Extracting a Ethernet Header from packet
int Extract_Ethernet_Header(char *buffer)
{
	struct eth_hdr *eth = (struct eth_hdr *)(buffer);		//Custom ethernet structure
/*
	printf("\nEthernet Header\n");
	printf("\t|Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->d_mac[0],eth->d_mac[1],eth->d_mac[2],eth->d_mac[3],eth->d_mac[4],eth->d_mac[5]);
	printf("\t|Source Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->s_mac[0],eth->s_mac[1],eth->s_mac[2],eth->s_mac[3],eth->s_mac[4],eth->s_mac[5]);
	printf("\t|Protocol : %d\n",eth->h_proto);
*/	
	return eth->h_proto;
}

#endif
