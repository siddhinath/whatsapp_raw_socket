#ifndef __USER_MSG_INTERFACE__
#define __USER_MSG_INTERFACE__ 1

#include "header_files.h"

int Check_User_Online(char *userid_c)
{
	FILE *fp;
	fp=fopen("src/users/online_users_list.txt","r");
	if(fp==NULL)
	{
		printf("Online file open error...\n");
	}
	char id[11],name[11];
	int t,flag=0;
	while(!feof(fp))
	{
		fscanf(fp,"%s%s%d",id,name,&t);
		if(feof(fp))
			break;
		if(strcmp(id,userid_c)==0)
			flag=1;
	}
	fclose(fp);
	if(flag==1)
		return 1;
	else 
	{
		//checking in route table
		route_table *temp = route_table_ll;
		while(temp!=NULL)
		{
			if(strcmp(temp->dest_userid,userid_c)==0)
				flag=2;
			temp=temp->next;
		}
		if(flag==2)
			return 1;
		else
			return 0;
	}
}

void Generate_Message_Packet(char *userid_outer,char *message, char *source_userid,int input_pkt_type)
{
	int flag = Check_User_Online(userid_outer);

	if(flag==1 || flag==2)//user is online
	{
		char find_through[20],mac_through[6];


		if(flag==2)
		{
//			printf("Getting mac from route table its indirectly connected...\n");
			route_table *temp = route_table_ll;
			while(temp!=NULL)
			{
				if(strcmp(temp->dest_userid,userid_outer)==0)
				{
					strcpy(find_through,temp->through_mac);
					break;
				}
				temp=temp->next;
			}
		}
		if(flag==1)
			strcpy(find_through,userid_outer);

		online_users *o_temp = online_list;
//		printf("Online users list[%s]...\n",find_through);
//		Display_ll(o_temp);
//		printf("Online users list end...\n");

		while(o_temp!=NULL)
		{
			if(strcmp(find_through,o_temp->s_userid)==0)
			{
				mac_through[0] = o_temp ->mac[0];
				mac_through[1] = o_temp ->mac[1];
				mac_through[2] = o_temp ->mac[2];
				mac_through[3] = o_temp ->mac[3];
				mac_through[4] = o_temp ->mac[4];
				mac_through[5] = o_temp ->mac[5];

//		printf("Copy [%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]\n",o_temp->mac[0],o_temp->mac[1],o_temp->mac[2],o_temp->mac[3],o_temp->mac[4],o_temp->mac[5]);
				break;
			}
			o_temp = o_temp->next;
		}
		
		int message_len = strlen(message);
		char packet[PKT_LEN];
		struct ethhdr *eth;
		eth=(struct ethhdr *)packet;

		struct myproto *o_proto;
		o_proto=(struct myproto *)(packet + sizeof(struct ethhdr));

		char *data;
		data  = (packet + sizeof(struct ethhdr) + sizeof(struct myproto));

		CreateEthernetHeader(mac_own_addr, mac_through, ETH_P_MY_PROTO,eth);
	
		//SETTING MYPROTO 
		o_proto->pkt_type = input_pkt_type;		        /* Type 4 for message sending */
	
		o_proto->payload  = message_len;	/* Data Size except two headers */

		strcpy(o_proto -> d_userid,userid_outer);

		strcpy(o_proto -> s_userid, source_userid); //source userid filled with entered mobile number

		//printf("[%s][%s]\n",userid_outer,source_userid);

		time(&o_proto -> rawtime);
	        
		strcpy(data,message);               /* Copy Data from user buffer to Packet */
	
		int total_len = sizeof(struct ethhdr)  + sizeof(struct myproto) + message_len;

//		printf("Packet Generated mac address=[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X][%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]\n",eth->h_source[0],eth->h_source[1],eth->h_source[2],eth->h_source[3],eth->h_source[4],eth->h_source[5],eth->h_dest[0],eth->h_dest[1],eth->h_dest[2],eth->h_dest[3],eth->h_dest[4],eth->h_dest[5]);
	    /* Actual packet sending using write system call. */
		if(!SendRawPacket(raw, packet, total_len))
		{
			perror("Error sending packet");
		}
		else
		{
			printf("Packet send success user message\n");
		}
	}//if flag==1 end 2
	else
	{
		printf("User online not found.\n");
	}
}

void Send_File_Generate_Packet(char *file_name,char *userid_l)
{
	printf("File sending in progress wait....\n");
	FILE *fp;
	fp = fopen(file_name,"r");
	if(fp == NULL)
	{
		printf("U entered file not found...\n");
		return ;
	}
	Generate_Message_Packet(userid_l, file_name , userid, 6);
	char fdata[100];
	int len;
	fp = fopen(file_name,"r");
	if(fp==NULL)
		printf("File cant open\n");
	while(!feof(fp))
	{
		fgets(fdata,100,fp);
		if(feof(fp))
			break;
		Generate_Message_Packet(userid_l, fdata, userid, 7);
		memset(fdata, 0,100);
	}
	Generate_Message_Packet(userid_l, fdata, userid, 8);
	printf("File send Successful...\n");
}

void UserMessageInterface()
{
        /* Accepting User message */
	while(1)
	{
		char userid_l[11],message[100];
		scanf("%s",userid_l);
		if(strcmp(userid_l,"file")==0)
		{
			//for sending file generate packet
			char file_name[100];
			printf("Enter File name (current dir or complete path)...\n");
			scanf("%s",file_name);
			printf("Enter User id to send file....\n");
			scanf("%s",userid_l);
			Send_File_Generate_Packet(file_name,userid_l);
		}
		else
		{
			fgets(message,100,stdin);
			Generate_Message_Packet(userid_l,message,userid,4);
			memset(userid_l,0,11);
			memset(message,0,100);
		}
	}
}

#endif 
