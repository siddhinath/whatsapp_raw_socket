#ifndef __PROCESS_PKT__
#define __PROCESS_PKT__ 1

#include "header_files.h"

void Process_Packet(char *buffer)
{
	int next_protocol=Extract_Ethernet_Header(buffer);

	/* Extracting myproto header */
	struct myproto *proto=(struct myproto *)(buffer + sizeof(struct ethhdr));
//	printf("Packet type=%d Payload=%d Name-size=%d\n",proto->pkt_type,proto->payload,proto->name_size);
	
	/* Check accepted packet is whatsapp protocol or not */
	if(next_protocol==3855 && strcmp(proto->s_userid,userid)!=0)
	{
		Extract_Data(buffer,proto);
	}
}

#endif
