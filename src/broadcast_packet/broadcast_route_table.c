#ifndef __BROADCAST_RT_
#define __BROADCAST_RT_ 1

void * Broadcast_Route_Table(void *vargp)
{
    while(1)
    {
    sleep(5);
   	char packet[2048];

	struct ethhdr *eth;
	eth=(struct ethhdr *)packet;

	struct myproto *o_proto;
	o_proto=(struct myproto *)(packet + sizeof(struct ethhdr));

	char *data;
	data  = (packet + sizeof(struct ethhdr) + sizeof(struct myproto));
    
	/* For broadcasting purpose */
	CreateEthernetHeader(mac_own_addr, mac_broadcast, ETH_P_MY_PROTO,eth);
    
	//Setting myproto header
	o_proto-> pkt_type = 3;
	//o_proto -> d_mac; Not set
	//o_proto -> s_mac; Not set

	//o_proto -> d_userid; At time reply filled
	strcpy(o_proto -> s_userid, userid); //source userid filled with entered mobile number

	time(&o_proto -> rawtime);

    //setting up the online address
    route_table *temp_rt = route_table_ll;

    char list_userid[1024];

    memset(list_userid,0,1024);

    while(temp_rt!=NULL)
    {
        strcat(list_userid, temp_rt -> dest_userid);
        strcat(list_userid," ");

        temp_rt = temp_rt -> next;
    }

    strcat(list_userid,"\0");
    strcpy(data,list_userid);

	o_proto -> payload = strlen(list_userid);

    int total_len = sizeof(struct ethhdr)  + sizeof(struct myproto) + strlen(list_userid);

	if(!SendRawPacket(raw, packet, total_len))
	{
	    perror("Error sending packet");
	}

    //printf("Route packet broadcasted...\n");
    }//while end
}

#endif