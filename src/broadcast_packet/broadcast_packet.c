#ifndef __BROADCAST_PKT_
#define __BROADCAST_PKT_ 1

#include "../header_files.h"

/* Function words in seperate thread. Broadcast own packet for online purpose.*/
void * Broadcast_Own_Mac(void *vargp)
{
	char packet[100];
	struct ethhdr *eth;
	eth=(struct ethhdr *)packet;
	
	struct myproto *o_proto;
	o_proto=(struct myproto *)(packet + sizeof(struct ethhdr));

	char *data;
	data  = (packet + sizeof(struct ethhdr) + sizeof(struct myproto));

	/* For broadcasting purpose */
	CreateEthernetHeader(mac_own_addr, mac_broadcast, ETH_P_MY_PROTO,eth);
	
	//Setting myproto header
	o_proto-> pkt_type = 1;
	//o_proto -> d_mac; Not set
	//o_proto -> s_mac; Not set

	//o_proto -> d_userid; At time reply filled
	strcpy(o_proto -> s_userid, userid); //source userid filled with entered mobile number

	time(&o_proto -> rawtime);
	o_proto -> payload = myname_len;
		
	strcpy(data,myname);

	int total_len = sizeof(struct ethhdr)  + sizeof(struct myproto) + myname_len;

	while(1)
	{
		if(!SendRawPacket(raw, packet, total_len))
		{
			perror("Error sending packet");
		}
		sleep(5);
	}
	return 0;
}

#endif
