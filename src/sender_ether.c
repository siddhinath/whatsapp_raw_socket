#include "header_files.h"
#include "receiver_raw.c"
#include <pthread.h>

int main(int argc, char **argv)
{
	char i_name[40];
	Fetch_Interface_Name(i_name);

	char u_name[20],u_mobile[20];
	printf("Enter user mobile number 10 digit...\n");
	scanf("%s",u_mobile);
	u_mobile[strlen(u_mobile)]='\0';

	printf("Enter user  name less than  10 character...\n");
	scanf("%s",u_name);
	u_name[strlen(u_name)]='\0';

	/* User name copy from command line argument */
	strcpy(myname,u_name);
	myname_len=strlen(myname);

	strcpy(userid,u_mobile);
	strcat(userid,"\0");
	
	pthread_t tid[4];

	pthread_create(&tid[2],NULL,Receiver,NULL);

	/* Create the raw socket */
	raw = CreateRawSocket(ETH_P_ALL);

	/* Bind raw socket to interface */
	BindRawSocketToInterface(i_name, raw, ETH_P_ALL);
	
	/* Fetching current machine mac address */
	struct ifreq ifreq_c;
	ifreq_c=GetOwnMacAddr(raw,i_name);

	/* First thread For broadcast and neighbour list send */
	pthread_create(&tid[0],NULL,Broadcast_Own_Mac,NULL);

/*
	if(fork()==0)
	{
		printf("In parent Child exec...\n");
		execl("route_table_ll","route_table_ll",argv[1]);
	}
	wait((int *)0);
*/

	/* User message interface */

	Initialize_Route_Table();
	printf("Route Table Entries...\n");
	Display_ll_RT(route_table_ll);

	pthread_create(&tid[3],NULL,Broadcast_Route_Table,NULL);

	UserMessageInterface();

	/* Closing socket file discriptor */
	close(raw);
	return 0;
}