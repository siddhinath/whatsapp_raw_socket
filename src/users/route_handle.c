#ifndef __RT_H__
#define __RT_H__ 1

#include "../header_files.h"

void Initialize_Route_Table()
{
    printf("Initializing route table...\n");
    
    if(route_table_ll == NULL)
    {
        route_table *temp;
        temp = newRT_Node();

        strcpy(temp->dest_userid,userid);
        strcpy(temp->dest_name,myname);

        temp->dest_mac[0] = mac_own_addr[0];
        temp->dest_mac[1] = mac_own_addr[1];
        temp->dest_mac[2] = mac_own_addr[2];
        temp->dest_mac[3] = mac_own_addr[3];
        temp->dest_mac[4] = mac_own_addr[4];
        temp->dest_mac[5] = mac_own_addr[5];
        

        strcpy(temp->through_userid,userid);
        strcpy(temp->through_name,myname);

        temp->through_mac[0] = mac_own_addr[0];
        temp->through_mac[1] = mac_own_addr[1];
        temp->through_mac[2] = mac_own_addr[2];
        temp->through_mac[3] = mac_own_addr[3];
        temp->through_mac[4] = mac_own_addr[4];
        temp->through_mac[5] = mac_own_addr[5];

        route_table_ll = temp;
        //printf("Set RT [%s:%s:[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]]\n",temp->dest_userid,temp->dest_name,temp->dest_mac[0],temp->dest_mac[1],temp->dest_mac[2],temp->dest_mac[3],temp->dest_mac[4],temp->dest_mac[5]);
    }
    
    else
    {
        printf("Route talbe already initialize.\n");
    }
    

}

#endif