#ifndef __LINK_LIST__
#define __LINK_LIST__ 1


#include<stdlib.h>
#include<stdio.h>
#include<time.h>

typedef struct online_users
{
        unsigned char s_userid[11];
        unsigned char name[10];
        unsigned char mac[6];
        time_t last_update;

		struct online_users *next;
		struct online_users *prev;
}online_users;


online_users *Newnode()
	{
		online_users *temp;
		temp=(online_users *)malloc(sizeof(online_users));
		temp->next=NULL;
		temp->prev=NULL;
		return temp;
	}			

online_users *AddFront(online_users *list,online_users *temp)
{
        if(list==NULL)
            return temp;
		temp->next = list;
        list->prev = temp;
		return temp;
}

online_users *DeleteFront(online_users *list)
{
		online_users *temp;
		if(list==NULL)
				return list;
		if(list->next==NULL)
			{
                free(list);
                return NULL;
            }
		else{
    		    temp=list->next;
	    	    free(list);
		        return temp;
            }
}


int Length(online_users *list)
{
        if(list==NULL)
            return 0;
        return 1 + Length(list->next);
}

void Display_ll(online_users *list)
{
		if(list==NULL)
			return;
		else
		{
            	printf("[%s]:[%s]:[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]:[%d]\n",list->s_userid, list->name, list->mac[0], list->mac[1], list->mac[2], list->mac[3], list->mac[4], list->mac[5], list->last_update);
			Display_ll(list->next);
		}
}

#endif

