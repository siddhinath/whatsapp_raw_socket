#ifndef __R_T__
#define __R_T__ 1

#include<stdlib.h>
#include<stdio.h>
#include<time.h>


typedef struct route_table
{
    unsigned char dest_userid[11];
    unsigned char dest_name[11];
    unsigned char dest_mac[6];

    unsigned char through_userid[11];
    unsigned char through_name[11];
    unsigned char through_mac[6];

    struct route_table *next;
    struct route_table *prev;
}route_table;


route_table *newRT_Node()
{
    route_table *temp;
    temp = (route_table *)malloc(sizeof(route_table));
    temp->next = NULL;
    temp->prev = NULL;
    return temp;
}

route_table *AddFront_RT(route_table *list,route_table *temp)
{
        if(list==NULL)
            return temp;
		temp->next = list;
        list->prev = temp;
		return temp;
}

route_table *DeleteFront_RT(route_table *list)
{
		route_table *temp;
		if(list==NULL)
				return list;
		if(list->next==NULL)
			{
                free(list);
                return NULL;
            }
		else{
    		    temp=list->next;
	    	    free(list);
		        return temp;
            }
}

int Length_RT(route_table *list)
{
        if(list==NULL)
            return 0;
        return 1 + Length_RT(list->next);
}

void Display_ll_RT(route_table *list)
{
		if(list==NULL)
			return;
		else
		{

		    printf("[%s:%s:[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]] ",list->dest_userid,list->dest_name,list->dest_mac[0],list->dest_mac[1],list->dest_mac[2],list->dest_mac[3],list->dest_mac[4],list->dest_mac[5]);
		    printf("[%s:%s:[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]]\n",list->through_userid,list->through_name,list->through_mac[0],list->through_mac[1],list->through_mac[2],list->through_mac[3],list->through_mac[4],list->through_mac[5]);

			Display_ll_RT(list->next);
		}
}

#endif