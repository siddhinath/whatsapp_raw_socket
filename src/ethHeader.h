//File contains ethernet header format.
//Without using system header use own structure
#ifndef _RAW_ETH_H 
#define _RAW_ETH_H 1

#define MAC_LEN 6

#define ETH_P_MY_PROTO 0x0F0F

//Decomal value 3855	//This value states that the (After header of ethernet there data present not a any other header like system define thats why this type of packet system kernet will not handle only my receiver will handle this tyoe of packet:

struct eth_hdr{
	unsigned char d_mac[MAC_LEN];
	unsigned char s_mac[MAC_LEN];
	short int h_proto;
};


#endif