#ifndef __MY_PROTOCOL__
#define __MY_PROTOCOL__ 1

#include<time.h>

struct myproto{
	short int pkt_type;
	unsigned char d_mac[MAC_LEN]; //actual destination mac address
	unsigned char s_mac[MAC_LEN]; // actual source mac address

	unsigned char d_userid[L_USERID];
	unsigned char s_userid[L_USERID];

	short int fname_length;

	time_t rawtime;			/* Stores rawtime  size=4  */

	short int payload;		/* Use for message packet  */	
};

#endif

