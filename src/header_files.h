#ifndef _HEADER_FILES_
#define _HEADER_FILES_ 1

char file_name_receiving[100];

int raw,raw_recv;	/* Sender_ether file use this ile discriptor to send packets only */
char mac_broadcast[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
char mac_other[6]={0x80,0xA5,0x89,0x3D,0x87,0xE1};

char userid[19];	//at the time of user entry it will be take input from command line argument

#define L_USERID 11

char mac_own_addr[6];
char myname[30];
int myname_len=0;

#define PKT_LEN 1024       

#include<fcntl.h>
#include<sys/stat.h>
#include<sys/stat.h>

#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include <time.h>

#include<sys/socket.h>
#include<netinet/ip.h>

#include<netinet/udp.h>
#include<arpa/inet.h>
#include<fcntl.h>
#include<netdb.h>
#include<netinet/ether.h>

#include<net/ethernet.h>
#include<sys/types.h>
#include<net/if.h>
#include<sys/ioctl.h>
#include<linux/if_ether.h>
#include<getopt.h>
#include<netpacket/packet.h>

#include "users/route_table.c"

#include"users/link_list.c"

route_table *route_table_ll = NULL;

#include "users/route_handle.c"

online_users *online_list = NULL;

#include "fetching_interface_name.c"

#include "ethHeader.h"
#include "myproto_header.h"
#include "create_socket.c"
#include "bind_socket.c"
#include "send_packet.c"
#include "create_ethernet_header.c"
#include "set_broadcast.c"
#include "get_own_mac_addr.c"
#include "extract_ethhdr.c"

#include "broadcast_packet/broadcast_packet.c"
#include "broadcast_packet/broadcast_route_table.c"

#include "user_message_interface.c"
#include "extract_data.c"
#include "process_packet.c"

//User Define
//#include "ipheader.h"
//#include "udpheader.h"
//#include "checksum.h"
//#include "getOwn_IP.h"


/* Methods prototype defined in different files   */
int CreateRawSocket(int );
int BindRawSocketToInterface(char *, int ,int );
int SendRawPacket(int, unsigned char *, int);
void CreateEthernetHeader(char *, char *,int, struct ethhdr *);
void SetBroadcastMacAddr(struct ethhdr *);
struct ifreq GetOwnMacAddr(int, char *);

void  *Broadcast_Own_Mac(void *vargp);
void UserMessageInterface();

int Extract_Ethernet_Header(char *);
void Extract_Data(char *,struct myproto *);
void Process_Packet(char *);





#endif

/*
	Packet Types
	1 -	Broadcasted packet				Process to set in list of online.
	2 -	Response to Broadcasted Packet im online	Not necessary to response this packet bcus every node broadcast own packet. 
	
	3 -	Neighbour online users list broadcast
	
	4 -	Message sending packet
	5 -	Message ack. 	Corresponding to its user
*/

