#ifndef _CREATE_ETH_HEADER_
#define _CREATE_ETH_HEADER_ 1

#include "header_files.h"

void CreateEthernetHeader(char *src_mac, char *dst_mac, int protocol,struct ethhdr *ethernet_header)
{
	//struct ethhdr *ethernet_header;
	//ethernet_header = (struct ethhdr *)malloc(sizeof(struct ethhdr));

	/* copy the Src mac addr */
	//memcpy(ethernet_header->h_source, (void *)ether_aton(src_mac), 6);
	ethernet_header->h_source[0]=src_mac[0];
	ethernet_header->h_source[1]=src_mac[1];
	ethernet_header->h_source[2]=src_mac[2];
	ethernet_header->h_source[3]=src_mac[3];
	ethernet_header->h_source[4]=src_mac[4];
	ethernet_header->h_source[5]=src_mac[5];

	/* copy the Dst mac addr */
	//memcpy(ethernet_header->h_dest, (void *)ether_aton(dst_mac), 6);
	ethernet_header->h_dest[0]=dst_mac[0];
	ethernet_header->h_dest[1]=dst_mac[1];
	ethernet_header->h_dest[2]=dst_mac[2];
	ethernet_header->h_dest[3]=dst_mac[3];
	ethernet_header->h_dest[4]=dst_mac[4];
	ethernet_header->h_dest[5]=dst_mac[5];

	/* copy the protocol */
	ethernet_header->h_proto = htons(protocol);
}

#endif
