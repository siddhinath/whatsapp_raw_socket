#ifndef __EXTRACT_DATA__
#define __EXTRACT_DATA__ 1

#include "header_files.h"
/*	Onlin_Users_file...		"online_users.txt"	[mobileno name mac lasttime] */

/* Works only display data from packet*/
void Display(char *buffer,int payload,char *data)
{
	printf("%.*s",payload, data); //it eill print only that much length data"payload"
	printf("\n");
}

/* Fetching Header and find out corresponding name display message*/
void Fetch_Message(char *buffer, struct ethhdr *eth, struct myproto *proto)
{
	char *data = (buffer + sizeof(struct ethhdr) + sizeof(struct myproto));  /* Points to the exact data of received packet.*/

	/* It will able to fetch corresponding name... form database */
	printf("[%s]:", proto -> s_userid);//
	//printf("[%.2X:%.2X:%.2X:%.2X:%.2X:%.2X]:",eth->h_source[0],eth->h_source[1],eth->h_source[2],eth->h_source[3],eth->h_source[4],eth->h_source[5]);
	Display(buffer, proto -> payload, data);
}	

void Add_User_In_History_File(struct online_users *temp)
{
	FILE *fp;
	fp=fopen("src/users/users_paired_list_history.txt","r");
	if(fp==NULL)
	{
		printf("Hostory file open error...\n");
	}
	char id[11],name[11];
	int t,flag=0;
	while(!feof(fp))
	{
		fscanf(fp,"%s%s%d",id,name,&t);
		if(feof(fp))
			break;
		if(strcmp(temp->s_userid,id)==0)
			flag=1;
	}
	fclose(fp);
	if(flag==0)
	{
		fp=fopen("src/users/users_paired_list_history.txt","a+");
		if(fp==NULL)
		{
			printf("Hostory file open error...\n");
		}
		fprintf(fp,"%s %s %d\n",temp->s_userid,temp->name,temp->last_update);
		fclose(fp);
		printf("Successfully add in history file.\n");
	}
}

void Update_online_list_file(online_users *list)
{
	FILE *fp;
	fp=fopen("src/users/online_users_list.txt","w");
	if(fp==NULL)
	{
		printf("Online file open error...\n");
	}
	printf("Adding in online list...\n");
	while(list!=NULL)
	{
		fprintf(fp,"%s %s %d\n",list->s_userid,list->name,list->last_update);
		list=list->next;
	}
	fclose(fp);
}

void Store_as_an_online(char *buffer, struct myproto *proto,struct ethhdr *eth)
{
	char *data = (buffer + sizeof(struct ethhdr) + sizeof(struct myproto));  /* Points to the exact data of received packet.*/

	online_users *temp;
	temp = online_list;
	int flag=0;
	while(temp!=NULL)
	{
		if(strcmp(temp->s_userid,proto->s_userid)==0)
			flag=1;
		temp=temp->next;
	}
	if(flag==0)
	{//it means user not in online list
		printf("Adding new user...\n");
		online_users *newnode = Newnode();
		strcpy(newnode->s_userid, proto->s_userid);
		strcat(newnode -> s_userid,"\0");
		strcpy(newnode->name, data);

		newnode -> mac[0] = eth->h_source[0];
		newnode -> mac[1] = eth->h_source[1];
		newnode -> mac[2] = eth->h_source[2];
		newnode -> mac[3] = eth->h_source[3];
		newnode -> mac[4] = eth->h_source[4];
		newnode -> mac[5] = eth->h_source[5];

		newnode ->last_update = proto ->rawtime;

		online_list = AddFront(online_list, newnode);

		Add_User_In_History_File(newnode);

		Update_online_list_file(online_list);

		printf("Newely added online list...\n");
		Display_ll(online_list);

	}
		
}
int Check_Route_Table_Already_Entry(char *rt_userid,struct myproto *proto)
{
    route_table *temp_rt = route_table_ll;
    int flag=0;
    while(temp_rt!=NULL)
    {
        if(strcmp(rt_userid, temp_rt->dest_userid)==0)
        {            //if match with dest in route table ...
            if(strcmp(temp_rt -> through_userid, proto -> s_userid)==0)
            {
                flag=1;
            }
            if(strcmp(rt_userid,userid)==0)
                flag=1;
        }
        temp_rt = temp_rt ->next;
    }
    if(flag==0)
        return 1;
    else
        return 0;
}
void Add_Entry_In_Route_Table(char *userid_rt, struct myproto *proto)
{
        route_table *temp;
        temp = newRT_Node();

        strcpy(temp->dest_userid,userid_rt);
        strcat(temp->dest_userid,"\0");

        strcpy(temp->through_userid,proto->s_userid);
        strcat(temp->through_userid,"\0");

        route_table_ll = AddFront_RT(route_table_ll, temp);

}

void Write_Table_In_File()
{
	FILE *fp;
	fp=fopen("src/users/route_table_list.txt","w+");

	route_table *temp = route_table_ll;
	while(temp!=NULL)
	{
		fprintf(fp,"%s %s\n",temp->dest_userid,temp->through_userid);
		temp=temp->next;
	}
}
void Route_Packet_Accept_Extract(char *buffer,struct ethhdr *eth,struct myproto *proto)
{
 	char *data;
	data  = (buffer + sizeof(struct ethhdr) + sizeof(struct myproto));
   
//    printf("Broadcasted route table packet accept...\n");
    //accept this packet and set route table again. with receive from which user and dest set by accepted userid
    int index=0;
    while(index < strlen(data)-1)
    {
        char rt_userid[11];
        memset(rt_userid,0,11);
        for(int i=0;i<11;i++)
            rt_userid[i] = data[i+index];
        rt_userid[10] = '\0';
        //printf("Fetched USERID=[%s] ",rt_userid);
        index = index+11;
        if(Check_Route_Table_Already_Entry(rt_userid,proto))
        {
            Add_Entry_In_Route_Table(rt_userid, proto);
            printf("Add New Entry in Route Table follwed by br packet successfull.\n");
            Store_as_an_online(buffer,proto,eth);
			Write_Table_In_File();
            Display_ll_RT(route_table_ll);
        }
    }
}



int Check_Its_Mine_or_Not(char *buffer,struct myproto *proto)
{
	//printf("Checking Both dest and own [%s] [%s]",proto->d_userid,userid);
	if(strcmp(proto->d_userid,userid)==0)
		return 1;
	else
		return 0;
}

void Handle_Forwarding_Packets(char *buffer, struct ethhdr *eth, struct myproto *proto)
{
//	if(strcmp(proto->s_userid,userid)!=0)

	if(eth->h_source[0]==mac_own_addr[0] &&eth->h_source[1]==mac_own_addr[1] &&eth->h_source[2]==mac_own_addr[2] && eth->h_source[3]==mac_own_addr[3] && eth->h_source[4]==mac_own_addr[4] && eth->h_source[5]==mac_own_addr[5])
	{
 		char *data;
		data  = (buffer + sizeof(struct ethhdr) + sizeof(struct myproto));
		//printf("Accept forward data=[%s][%s][%s]",proto->s_userid,proto->d_userid,data);

		Generate_Message_Packet(proto->d_userid,data,proto->s_userid,4);
	}
}

void Receive_File_And_Store(char *buffer,struct ethhdr *eth, struct myproto *proto)
{
	char *data = (buffer + sizeof(struct ethhdr) + sizeof(struct myproto));  /* Points to the exact data of received packet.*/
	char path[100]={"src/received_files/"};

	if(proto->pkt_type == 6)
	{
		printf("File Receiving in process...\n");
		strcat(path,data);
		strcpy(file_name_receiving, path);
		FILE *fp = fopen(file_name_receiving,"w+");
		if(fp == NULL)
		{
			printf("File not create error... 6\n");
			exit(0);
		}
		fclose(fp);
		printf("File Create_Successfully...[%s][%s]\n",file_name_receiving,data);
	}
	else if(proto ->pkt_type == 7)
	{
		FILE *fp;
		fp  = fopen(file_name_receiving, "a+");
		if(fp == NULL)
		{
			printf("File open error 7\n");
		}
		else
		{

			int  w= fwrite(data, 100, 1,fp);
			//printf("File Append [%d]\n",w);
			fclose(fp);
		}
	}
	else if(proto->pkt_type == 8)
	{
		printf("[%s] File Read Successfully\n",file_name_receiving);
		memset(file_name_receiving,0,100);
	}
}

void Extract_Data(char *buffer,struct myproto *proto)
{
	struct ethhdr *eth;
	eth =(struct ethhdr *) buffer;	/* Allocates the ethernet header size memory to eht structure	*/

	int remaining_data = proto->payload;

	if(proto->pkt_type == 1)	/* Broadcasted mac addr packet. */
	{
		//Handle all task from here
		/*
		1. Check and store it for online user list and update route table
		*/
		Store_as_an_online(buffer, proto,eth);
	}
	else if(proto->pkt_type == 3) /* Broadcast neighbour list. */ //accept broadcasted neighbour packet...
	{
		/* Here broadcasting neighbour packet */

		Route_Packet_Accept_Extract(buffer,eth,proto);
	}
	else if(proto->pkt_type == 4)/* Message Type. */
	{
		if(Check_Its_Mine_or_Not(buffer,proto))
			Fetch_Message(buffer, eth, proto);
		else
		{
			printf("Forwarding a packet as per userid address...\n");
			Handle_Forwarding_Packets(buffer,eth,proto);
		}
	}
	else if(proto->pkt_type == 5)/* Message ACK. */
	{

	}
	else if(proto->pkt_type == 6 )
	{
		/* file share first incoming */
		Receive_File_And_Store(buffer, eth, proto);
	}
	else if(proto->pkt_type == 7)
	{
		// file sending progress...
		Receive_File_And_Store(buffer, eth, proto); //use in resume program...
	}
	else if(proto->pkt_type == 8)
	{
		// file sending end message...
		Receive_File_And_Store(buffer, eth, proto);
	}
	
}

#endif
